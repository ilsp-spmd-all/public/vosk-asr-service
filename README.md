# Vosk ASR service

This README contains instructions on how to run the **websockets** client. For a **gRPC** client go to the [grpc](grpc) folder. For instructions on how to deploy a (gRPC or websockets) Vosk server, go to the [server](server) folder.

## Installation

In order to run the ws client you need to install websockets. This package requires a Python version >= 3.7 (and <= 3.10), so it's best if you first create and activate a virtual environment. There are several ways to create a virtual environment; for instance:

```
python3.9 -m venv env
source env/bin/activate
```

Once you are in the virtual environment, install the websockets package via the following commands:

```
pip install websockets==10.3
```

or 

```
pip install -r requirements.txt
```

## How to run transcribe_wav.py

This client connects to an existing Vosk WS server and transcribes a given .wav file. The only required input is the audio file path; the hostname and port have default values. See:

```
python transcribe_wav.py -h
```

which outputs:

```
usage: transcribe_wav.py [-h] [--server-host SERVER_HOST]
                         [--server-port SERVER_PORT] --audio-file AUDIO_FILE
                         [--full-json-response]

Vosk client that allows a user to transcribe an audio file

optional arguments:
  -h, --help            show this help message and exit
  --server-host SERVER_HOST
                        The WS server hostname (default: chomsky.ilsp.gr)
  --server-port SERVER_PORT
                        The WS server port (default: 12700)
  --audio-file AUDIO_FILE, --wav AUDIO_FILE
                        Path to the wav file that needs to be transcribed
  --full-json-response  Use this flag to show the full server json response,
                        which includes partial results and alignments. (default: False)
```

For instance, if you run:

```
python transcribe_wav.py --wav test.wav
```

the expected output is:

```
one zero zero zero one
nine oh two one oh
zero one eight zero three
```

whereas if you use the `--full-json-response` flag at the end of the command, it will output:

```
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': ''}
{'partial': 'one'}
{'partial': 'one'}
{'partial': 'one zero zero'}
{'partial': 'one zero zero'}
{'partial': 'one zero zero zero'}
{'partial': 'one zero zero zero'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'partial': 'one zero zero zero one'}
{'result': [{'conf': 1.0, 'end': 1.14, 'start': 0.81, 'word': 'one'}, {'conf': 1.0, 'end': 1.53, 'start': 1.14, 'word': 'zero'}, {'conf': 1.0, 'end': 1.95, 'start': 1.53, 'word': 'zero'}, {'conf': 1.0, 'end': 2.34, 'start': 1.95, 'word': 'zero'}, {'conf': 1.0, 'end': 2.61, 'start': 2.34, 'word': 'one'}], 'text': 'one zero zero zero one'}
{'partial': ''}
{'partial': 'nine out'}
{'partial': ''}
{'partial': ''}
{'partial': 'nine oh two one oh'}
{'partial': 'nine oh two one oh'}
{'partial': 'nine oh two one oh'}
{'partial': 'nine oh two one oh'}
{'partial': 'nine oh two one oh'}
{'partial': 'nine oh two one oh'}
{'result': [{'conf': 1.0, 'end': 4.17, 'start': 3.93, 'word': 'nine'}, {'conf': 1.0, 'end': 4.29, 'start': 4.17, 'word': 'oh'}, {'conf': 1.0, 'end': 4.53, 'start': 4.29, 'word': 'two'}, {'conf': 1.0, 'end': 4.68, 'start': 4.53, 'word': 'one'}, {'conf': 1.0, 'end': 4.98, 'start': 4.68, 'word': 'oh'}], 'text': 'nine oh two one oh'}
{'partial': ''}
{'partial': ''}
{'partial': 'zero'}
{'partial': 'zero one'}
{'partial': 'zero one'}
{'partial': 'zero one eight'}
{'partial': 'zero one eight'}
{'partial': 'zero one eight thorough'}
{'partial': 'zero one eight thorough'}
{'result': [{'conf': 1.0, 'end': 6.69, 'start': 6.24, 'word': 'zero'}, {'conf': 1.0, 'end': 6.9, 'start': 6.69, 'word': 'one'}, {'conf': 1.0, 'end': 7.17, 'start': 6.9, 'word': 'eight'}, {'conf': 1.0, 'end': 7.5, 'start': 7.17, 'word': 'zero'}, {'conf': 1.0, 'end': 7.98, 'start': 7.5, 'word': 'three'}], 'text': 'zero one eight zero three'}
```

The ideal audio sample rate is **16kHz**. However, the server accepts other sample rates too. For instance, the same audio file in 8kHz outputs:

```
python transcribe_wav.py --wav test8.wav

one zero zero zero one
nine oh two one oh
zero one eight thoreau three
```