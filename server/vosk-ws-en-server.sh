#!/bin/bash

## set the listening port in host machine (default port: 2700)
EXT_PORT=$1
if [ -z "$EXT_PORT" ]; then
    EXT_PORT=2700
fi

## the model that will be deployed. It can be any model from the list: https://alphacephei.com/vosk/models
MODEL_NAME=$2
if [ -z "$MODEL_NAME" ]; then
    MODEL_NAME="vosk-model-en-us-0.22"
fi

# download file if it doesn't exist
if [ ! -f ${MODEL_NAME}.zip ]
then
    wget https://alphacephei.com/vosk/models/${MODEL_NAME}.zip
fi
# check exit code
if [ $? -ne 0 ]; then
   exit 1
fi

# extract model if folder doesn't exist
unzip ${MODEL_NAME}.zip

MODEL_ABS_PATH=$(pwd)/${MODEL_NAME}

docker run -d \
    -p ${EXT_PORT}:2700 \
    -v ${MODEL_ABS_PATH}:/opt/vosk-model-en/model \
    alphacep/kaldi-en:latest
