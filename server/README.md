## Deploying Vosk Server with custom models

This folder contains info and scripts for deploying the Vosk server with a model from this [list](https://alphacephei.com/vosk/models).

**Deploy scripts:**  
- **Websocket** version: `vosk-ws-en-server.sh`
- **gRPC** version: `vosk-grpc-en-server.sh`

The default ports are `2700` for websocket version and `5001` for the grpc one.  
If a different port is needed, just pass it as the **first** argument of the script. For example:

`./vosk-ws-en-server.sh 11111`

Similarly, the default [model](https://alphacephei.com/vosk/models) is `en-us-0.22`. If you would like to deploy a different model, pass the name as the **second** argument. For example:

`./vosk-ws-en-server.sh 2700 vosk-model-small-en-us-0.15`
