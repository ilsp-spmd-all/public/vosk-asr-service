"""
Websockets Vosk client
"""
import argparse
import asyncio
import json
import wave

import websockets


async def transcribe(uri, audio_fname, full_json_response):
    async with websockets.connect(uri) as websocket:
        wf = wave.open(audio_fname, "rb")
        await websocket.send(
            '{ "config" : { "sample_rate" : %d} }' % (wf.getframerate())
        )
        buffer_size = int(wf.getframerate() * 0.2)  # 0.2 seconds of audio
        while True:
            data = wf.readframes(buffer_size)

            if len(data) == 0:
                break

            await websocket.send(data)
            response = json.loads(await websocket.recv())
            if full_json_response:
                print(response)
            elif "text" in response:
                print(response["text"])
        await websocket.send('{"eof" : 1}')
        final_response = json.loads(await websocket.recv())
        if full_json_response:
            print(final_response)
        elif "text" in final_response:
            print(final_response["text"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Vosk WS client that allows a user to transcribe an audio file",
    )
    parser.add_argument(
        "--server-host",
        type=str,
        help="The WS server hostname",
        default="chomsky.ilsp.gr",
    )
    parser.add_argument(
        "--server-port", type=str, help="The WS server port", default="12700"
    )
    parser.add_argument(
        "--audio-file",
        "--wav",
        type=str,
        help="Path to the wav file that needs to be transcribed",
        required=True,
    )
    parser.add_argument(
        "--full-json-response",
        action="store_true",
        default=False,
        help="Use this flag to show the full server json response, which includes partial results and alignments.",
    )

    args = parser.parse_args()

    asyncio.run(
        transcribe(
            f"ws://{args.server_host}:{args.server_port}",
            args.audio_file,
            args.full_json_response,
        )
    )
