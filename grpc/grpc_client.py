import argparse

import grpc
import stt_service_pb2
import stt_service_pb2_grpc

CHUNK_SIZE = 4000


def gen(audio_file_name, sample_rate=16000, partial_results=False):
    """
    Sets the specs for streaming recognition and sends audio chunks
    """
    specification = stt_service_pb2.RecognitionSpec(
        partial_results=partial_results,
        audio_encoding="LINEAR16_PCM",
        sample_rate_hertz=sample_rate,
        enable_word_time_offsets=True,
        max_alternatives=0,  # 5,
    )
    streaming_config = stt_service_pb2.RecognitionConfig(specification=specification)

    yield stt_service_pb2.StreamingRecognitionRequest(config=streaming_config)

    with open(audio_file_name, "rb") as fhandler:
        data = fhandler.read(CHUNK_SIZE)
        while data != b"":
            yield stt_service_pb2.StreamingRecognitionRequest(audio_content=data)
            data = fhandler.read(CHUNK_SIZE)


def transcribe(
    audio_file_name,
    server_name,
    server_port,
    full_response,
    partial_results,
    sample_rate,
):
    """
    Starts grpc connection and transcribes an audio file
    """
    channel = grpc.insecure_channel(f"{server_name}:{server_port}")
    stub = stt_service_pb2_grpc.SttServiceStub(channel)
    it = stub.StreamingRecognize(
        gen(audio_file_name, sample_rate=sample_rate, partial_results=partial_results)
    )

    try:
        for r in it:
            try:
                if full_response:
                    for alternative in r.chunks[0].alternatives:
                        print(alternative)
                        # print(alternative.text)
                        # print('alternative_confidence: ', alternative.confidence)
                        # print('words: ', alternative.words)
                elif r.chunks[0].final:
                    print(r.chunks[0].alternatives[0].text)
            except LookupError:
                print("No chunks available")
    except grpc._channel._Rendezvous as err:
        print("Error code %s, message: %s" % (err._state.code, err._state.details))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Vosk GRPC client that allows a user to transcribe an audio file",
    )
    parser.add_argument(
        "--server-host",
        type=str,
        help="The grpc server hostname",
        default="chomsky.ilsp.gr",
    )
    parser.add_argument(
        "--server-port", type=str, help="The grpc server port", default="12701"
    )
    parser.add_argument(
        "--wav",
        "--audio-file",
        required=True,
        type=str,
        help="Path to the wav file that needs to be transcribed",
    )
    parser.add_argument(
        "--sample-rate", type=int, default=16000, help="The audio sample rate",
    )
    parser.add_argument(
        "--full-json-response",
        action="store_true",
        default=False,
        help="Use this flag to show the full server json response, which includes \
            word alignments and confidence scores.",
    )
    parser.add_argument(
        "--partial-results",
        action="store_true",
        default=False,
        help="Use this flag to show the partial results. It only works in combination \
            with full-json-response.",
    )

    args = parser.parse_args()

    transcribe(
        audio_file_name=args.wav,
        server_name=args.server_host,
        server_port=args.server_port,
        full_response=args.full_json_response,
        partial_results=args.partial_results,
        sample_rate=args.sample_rate,
    )
