# Vosk GRPC client

Similar to [`transcribe_wav.py`](../transcribe_wav.py) that connects to a WS server, `grpc_client.py` transcribes an audio file using a Vosk GRPC server.

## Installation

First, you need run the Makefile (`make`) inside the `grpc` folder to generate the two following files:

- stt_service_pb2.py
- stt_service_pb2_grpc.py


## How to run grpc_client.py

This client connects to an existing Vosk GRPC server and transcribes a given .wav file. The only required input is the audio file path; the hostname and port have default values. See:

```
python grpc_client.py -h
```

which outputs:

```
usage: grpc_client.py [-h] [--server-host SERVER_HOST]
                      [--server-port SERVER_PORT] --wav WAV
                      [--sample-rate SAMPLE_RATE] [--full-json-response]
                      [--partial-results]

Vosk GRPC client that allows a user to transcribe an audio file

optional arguments:
  -h, --help            show this help message and exit
  --server-host SERVER_HOST
                        The grpc server hostname (default: chomsky.ilsp.gr)
  --server-port SERVER_PORT
                        The grpc server port (default: 12701)
  --wav WAV, --audio-file WAV
                        Path to the wav file that needs to be transcribed
                        (default: None)
  --sample-rate SAMPLE_RATE
                        The audio sample rate (default: 16000)
  --full-json-response  Use this flag to show the full server json response,
                        which includes word alignments and confidence scores.
                        (default: False)
  --partial-results     Use this flag to show the partial results. It only
                        works in combination with full-json-response.
                        (default: False)
```

For instance, if you run:

```
python grpc_client.py --wav ../test.wav
```

the expected output is:

```
one zero zero zero one
nine oh two one oh
zero one eight zero three
```

whereas for an audio file with a sample rate of 8kHz, use the `--sample-rate` flag:

```
python grpc_client.py --wav ../test8.wav --sample-rate 8000
```

If you use the `--full-json-response` flag at the end of the command, it will output the confidence scores and alignments:

```
text: "one zero zero zero one"
confidence: 1.0
words {
  start_time {
    nanos: 810000000
  }
  end_time {
    seconds: 1
    nanos: 140000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 140000000
  }
  end_time {
    seconds: 1
    nanos: 530000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 530000000
  }
  end_time {
    seconds: 1
    nanos: 950000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 950000000
  }
  end_time {
    seconds: 2
    nanos: 340000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 2
    nanos: 340000000
  }
  end_time {
    seconds: 2
    nanos: 610000000
  }
  word: "one"
  confidence: 1.0
}

text: "nine oh two one oh"
confidence: 1.0
words {
  start_time {
    seconds: 3
    nanos: 930000000
  }
  end_time {
    seconds: 4
    nanos: 170000000
  }
  word: "nine"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 170000000
  }
  end_time {
    seconds: 4
    nanos: 290000000
  }
  word: "oh"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 290000000
  }
  end_time {
    seconds: 4
    nanos: 530000000
  }
  word: "two"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 530000000
  }
  end_time {
    seconds: 4
    nanos: 680000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 680000000
  }
  end_time {
    seconds: 4
    nanos: 980000000
  }
  word: "oh"
  confidence: 1.0
}

text: "zero one eight zero three"
confidence: 1.0
words {
  start_time {
    seconds: 6
    nanos: 240000000
  }
  end_time {
    seconds: 6
    nanos: 690000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 6
    nanos: 690000000
  }
  end_time {
    seconds: 6
    nanos: 900000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 6
    nanos: 900000000
  }
  end_time {
    seconds: 7
    nanos: 170000000
  }
  word: "eight"
  confidence: 1.0
}
words {
  start_time {
    seconds: 7
    nanos: 170000000
  }
  end_time {
    seconds: 7
    nanos: 500000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 7
    nanos: 500000000
  }
  end_time {
    seconds: 7
    nanos: 980000000
  }
  word: "three"
  confidence: 1.0
}
```

Further adding the flag --partial-results will output the partial text:

```
text: "one"

text: "one"

text: "one"

text: "one"

text: "one zero zero"

text: "one zero zero"

text: "one zero zero"

text: "one zero zero"

text: "one zero zero zero"

text: "one zero zero zero"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"

text: "one zero zero zero one"
confidence: 1.0
words {
  start_time {
    nanos: 810000000
  }
  end_time {
    seconds: 1
    nanos: 140000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 140000000
  }
  end_time {
    seconds: 1
    nanos: 530000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 530000000
  }
  end_time {
    seconds: 1
    nanos: 950000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 1
    nanos: 950000000
  }
  end_time {
    seconds: 2
    nanos: 340000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 2
    nanos: 340000000
  }
  end_time {
    seconds: 2
    nanos: 610000000
  }
  word: "one"
  confidence: 1.0
}

text: "nine out"

text: "nine out"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"

text: "nine oh two one oh"
confidence: 1.0
words {
  start_time {
    seconds: 3
    nanos: 930000000
  }
  end_time {
    seconds: 4
    nanos: 170000000
  }
  word: "nine"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 170000000
  }
  end_time {
    seconds: 4
    nanos: 290000000
  }
  word: "oh"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 290000000
  }
  end_time {
    seconds: 4
    nanos: 530000000
  }
  word: "two"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 530000000
  }
  end_time {
    seconds: 4
    nanos: 680000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 4
    nanos: 680000000
  }
  end_time {
    seconds: 4
    nanos: 980000000
  }
  word: "oh"
  confidence: 1.0
}

text: "zero"

text: "zero"

text: "zero one"

text: "zero one"

text: "zero one eight"

text: "zero one eight"

text: "zero one eight"

text: "zero one eight"

text: "zero one eight thorough"

text: "zero one eight thorough"

text: "zero one eight thorough"

text: "zero one eight zero three"
confidence: 1.0
words {
  start_time {
    seconds: 6
    nanos: 240000000
  }
  end_time {
    seconds: 6
    nanos: 690000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 6
    nanos: 690000000
  }
  end_time {
    seconds: 6
    nanos: 900000000
  }
  word: "one"
  confidence: 1.0
}
words {
  start_time {
    seconds: 6
    nanos: 900000000
  }
  end_time {
    seconds: 7
    nanos: 170000000
  }
  word: "eight"
  confidence: 1.0
}
words {
  start_time {
    seconds: 7
    nanos: 170000000
  }
  end_time {
    seconds: 7
    nanos: 500000000
  }
  word: "zero"
  confidence: 1.0
}
words {
  start_time {
    seconds: 7
    nanos: 500000000
  }
  end_time {
    seconds: 7
    nanos: 980000000
  }
  word: "three"
  confidence: 1.0
}
```